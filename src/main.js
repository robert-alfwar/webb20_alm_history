const fs = require("fs");
const isAnagram = require('./anagram');


const names = fs.readFileSync('./data/name.txt').toString().split('\n');

const anagrams = names.reduce((acc, cur) => {
    if (isAnagram(cur, 'and')) {
        acc.push(cur)
    }
    return acc
}, []);

console.log(anagrams)