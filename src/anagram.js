// TODO add anagram function

const validateText = (text) => {
    if (text.length > 0) return true;
    return false;
}

const sortLetters = (text) => {
    return text.split('').sort().join('');
}

const isAnagram = (firstText, secondText) => {
    if (validateText(secondText) === true) {
        return sortLetters(firstText) === sortLetters(secondText)
    }
    console.log("not valid length");
    return false
}

module.exports = isAnagram